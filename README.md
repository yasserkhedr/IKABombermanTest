Yasser Khedr's IKA Test

Unreal version: 4.18.1

Time spent on the test: 14 hours.
* I ran into technical difficulties due to my old laptop and git not pushing, but I was able to solve all the issues. 
* I worked on this over the course of a few days, using up a few hours each day.

Controls:
Player one:
* W - Up
* S - Down
* A - Left
* D - Right
* F - Detonate Bomb
* Space bar - Place bomb

Player two:
* Up arrow key - Up
* Down arrow key - Down
* Left arrow key - Left
* Right arrow key - Right
* numpad 0 - Detonate Bomb
* Right ctrl - Place bomb

Power up colors map:
* Faster run powerup - Green grass cube
* More bombs powerup - Mealic rust brown cube
* Longer range powerup - Gray rock cube
* Remote control powerup - Glowing blue cube

* Player one - Gold cube
* Player two - Copper cube

* End block - Wood brown plane
* Indestrucible wall - Metal wall
* Destructible wall - Brick wall
* Floor - Metal floor

Level generation:
* In the level blueprint of LevelGeneration map.
* Change the row and column default values to change the size of the map, can only be an odd number.

Improvements:
* Moving forward with improvements the first improvement would be to create a map that is held in a matrix or 2d int array that maps everything to avoid having to do line traces. But the way the bomb would operate and check its surroundings would still be similar in nature, and does not bring any immediate benefits other than eliminating line traces. Also, due to the nature of this test with the time constraint, a simpler approach of just doing line traces had to suffice.
* The current implementation does not utilize any sort of UI Manager, instead it is very sequential and fragile; a UI system that handles all widgets popping up and disappearing would be ideal.
* The current levels are being loaded through an open level command, this is not ideal and level streaming is a more elegant solution.
* The level generation is adequate for what it is, but perhaps a better algorithm and a port to C++ would be better on performance considering that blueprints run slower than code because blueprints utilize a virtual machine to run its scripts.
* Would avoid using GetAllActorsOfClass, this function can be pretty expensive; I made sure that whenever I used it the command would only run once to get what I need.
* Adding a better pickup system that would append to an array that the character processes once rather than calling a function directly on the player would benefit the game by allowing us to have more dynamic pickups. However, I do not know if pickups should stack; I assumed they can stack but if they are not supposed to stack the implementation mentioned earlier would be a far superior implementation.
* In terms of AI, I had an idea in mind but I did not have enough time to implement; the idea I had with the current implementation was to give the AI the same pawn class and make it go in a random direction when hitting a wall while doing line traces on each new block its on to see what it is near. This is a very primitive AI but it would get the job done. If I were to make a better AI, I’d implement the 2D array map mentioned above and give the AI a clear vision on everything happening. If the level generation is too big, I’d narrow the AI’s vision to a certain row by column and perhaps the distance it has from the other players. The AI would process the blocks and take the ideal path to blow up destructible walls and avoid any bombs placed, this would even give the AI the ability to stay away from players if it wishes to play a defensive game.

I operated on the assumption of many things, I made my best judgement based on the video of bomber man but I apologize if there is a behavior that was not intended but not clearly specified in the requirements list.

Examples:
* What happens if you enter the blast radius after the bomb explosion but the sprite is still there? Do you die? For now I assumed no.
* Does a player collide with bombs? In my implementation, yes and no; once you spawn a bomb the bomb is technically on the same block as you but you can move away from it, once you try to revisit the block you won’t be able to.
