/**
	This is the base C++ class for the player character, which is also called the pawn.
	There's no point from inheriting from the Character class as it will bloat the code
	with unnecessary functionality; seeing as I have decided to approach the game with
	a grid based movement system rather than use the Character movement component which
	happens to be expensive and have replication code that is not required by the
	parameters of this project.

	A blueprint file will be created that inherits from this C++ class.
*/

#pragma once

#include "CoreMinimal.h"
#include "Bombs/IKA_Bomb.h"
#include "Pickup/IKA_Pickup_Base.h"
#include "IKA_Pawn.generated.h"

UCLASS()
class IKATEST_API AIKA_Pawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AIKA_Pawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/** Indicated that this player is now in remote control mode, 
	bomb count will be reset to one and set back when mode is over. */
	void StartRemoteControlMode();

	/** End the remote control mode and restore original available bomb count. */
	void EndRemoteControlMode();

	/** Is the player currently in remote control mode. */
	bool GetRemoteControlMode() { return _bPlayerHasRemoteControl ? true : false; };

	/** Modify the number of bombs. */
	void ModifyBombCount(int32 countModificationValue);
	
	/** Modify the bomb explosion range. */
	void ModifyBombRange(int32 rangeModficationValue);

	/** Modify the player's movement speed. */
	void ModifyPlayerSpeed(float speedModificationValue);

	/** Player killed function called by explosions. */
	void PlayerKilled(APawn* instigator);

	/** Respawn player event, called by the player killed function. */
	void RespawnPlayer();

	/** Place and explosive, spawns it from the TSubclass specified. */
	UFUNCTION(BlueprintCallable, Category = "Player")
	void PlaceBomb();

	/** Detonates remote bomb, auto detonates at end of mode timer. */
	UFUNCTION(BlueprintCallable, Category = "Player")
	void DetonateRemoteBomb();

	/** Sets the name of the player. */
	UFUNCTION(BlueprintCallable, Category = "Player")
	void SetPlayerName(FString playerName) { _playerName = playerName; };

	/** Modify the player's score. */
	UFUNCTION(BlueprintCallable, Category = "Player")
	void ModifyPlayerScore(int32 modification) { playerScore += modification; };

	/** BlueprintImplementableEvent to call when a player dies to reset their movement (fixes a bug). */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Player")
	void ResetPlayerMovement();

	/** The speed of the player. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	float playerSpeed;

	/** Current location of the player. */
	UPROPERTY(BlueprintReadWrite, Category = "Player")
	FVector currentLocation;

	/** Respawn point of the player. */
	UPROPERTY(BlueprintReadWrite, Category = "Player")
	FVector respawnPoint;

	/** The player number. e.g. Player 1 or Player 2 etc. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	int32 playerNumber;

	/** Number of player lives left. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	int32 playerLives;
	
	/** Score of the player. */
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Player")
	int32 playerScore;

	/** Bomb to spawn. */
	UPROPERTY(EditAnywhere, Category = "Player")
	TSubclassOf<AIKA_Bomb> bombToUse;

private:
	/** 
		I would process the pickups and store them in an array if the specifications said that
		pickups cannot stack, however I am under the assumption that they can stack.
		In the event that I process pickups I'd make a new function that processed the pickup 
		class and added it to the TArray then apply the effect to the specific player.
	*/
	//TArray<AIKA_Pickup_Base> pickUpList;

	/** Name of the player. */
	UPROPERTY(VisibleAnywhere, Category = "Player")
	FString _playerName;

	/** Number of bombs player can drop. */
	UPROPERTY(VisibleAnywhere, Category = "Player")
	int32 _availableBombs;

	/** A holder for the number of bombs while remote mode is active. */
	int32 _availableBombsHold;
	
	/** What is the current player's bomb range, injected to the bombs the player places. */
	UPROPERTY(VisibleAnywhere, Category = "Player")
	int32 _playerBombRange;
	
	/** Is the player in remote control mode? */
	UPROPERTY(VisibleAnywhere, Category = "Player")
	uint8 _bPlayerHasRemoteControl : 1;

	/** Need a reference of the remote bomb to detonate. */
	AIKA_Bomb* _remoteBomb;

	/** Time handle for the remote control mode. */
	FTimerHandle _remoteTimer;

	/** How long the remote control mdoe will last. */
	float _remoteModeTimer;
};
