// Fill out your copyright notice in the Description page of Project Settings.

#include "IKA_Gamemode.h"

AIKA_Gamemode::AIKA_Gamemode() :
	playerOne(nullptr),
	playerTwo(nullptr),
	destructibleBlocksLeft(27),
	gameTime(500.f),
	timeRemaining(0.f)
{

}

void AIKA_Gamemode::BeginPlay()
{
	Super::BeginPlay();

	UWorld* pWorld = GetWorld();
	if (pWorld)
	{
		pWorld->GetTimerManager().SetTimer(gameTimer, this, &AIKA_Gamemode::GameOver, gameTime);
	}
}

void AIKA_Gamemode::DecreaseBlocksLeft()
{
	if (destructibleBlocksLeft > 0)
	{
		destructibleBlocksLeft--;
	}
}

void AIKA_Gamemode::IncreaseBlocksLeft()
{
	destructibleBlocksLeft++;
}

