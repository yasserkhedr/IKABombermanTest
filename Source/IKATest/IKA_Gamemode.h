/** 
	This is the base C++ class for the gamemode.
	A blueprint file will be created that inherits from this C++ class.
*/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "IKA_Gamemode.generated.h"

class AIKA_Pawn;

/**
 * 
 */
UCLASS()
class IKATEST_API AIKA_Gamemode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AIKA_Gamemode();

	virtual void BeginPlay() override;

	/** Game over event, implemented in blueprints. Pauses the game mostly. */
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Game")
	void GameOver();

	/** Decrease blocks left counter for end game conditions. */
	void DecreaseBlocksLeft();

	/** Increase blocks left counter for level generation purposes. */
	UFUNCTION(BlueprintCallable, Category = "Level Generation")
	void IncreaseBlocksLeft();

	/** Reference of player one to be used by other classes. */
	UPROPERTY(BlueprintReadWrite)
	AIKA_Pawn* playerOne;

	/** Reference of player two to be used by other classes. */
	UPROPERTY(BlueprintReadWrite)
	AIKA_Pawn* playerTwo;
	
	/** Number of destructible blocks left. */
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	int32 destructibleBlocksLeft;

	/** Time handle for the game time running. */
	UPROPERTY(BlueprintReadWrite)
	FTimerHandle gameTimer;

	/** How long should the game last. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Game")
	float gameTime;

	/** Time left till end of the game. */
	UPROPERTY(BlueprintReadWrite)
	float timeRemaining;
};
