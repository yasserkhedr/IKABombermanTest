/** 
	Controller class, not being used; could be used in the future to grow the project.
	Intention was for it to handle the inputs and send them over but the time constraints
	had other plans.
*/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "IKA_Controller.generated.h"

/**
 * 
 */
UCLASS()
class IKATEST_API AIKA_Controller : public APlayerController
{
	GENERATED_BODY()
	
	
	
	
};
