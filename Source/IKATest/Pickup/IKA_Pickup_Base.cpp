// Fill out your copyright notice in the Description page of Project Settings.

#include "IKA_Pickup_Base.h"


// Sets default values
AIKA_Pickup_Base::AIKA_Pickup_Base()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// void AIKA_Pickup_Base::OnOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
// {
// 	// OtherActor is the actor that triggered the event. Check that is not ourself 
// 	APawn* pPawn = Cast<APawn>(OtherActor);
// 
// 	if (pPawn)
// 	{
// 		PerformPickupAbility(pPawn);
// 	}
// 
// 	Destroy();
// }

void AIKA_Pickup_Base::PerformPickupAbility(APawn* collidedPawn)
{
	// Virtual class to be overridden.
}

// Called when the game starts or when spawned
void AIKA_Pickup_Base::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AIKA_Pickup_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

