/**
	Remote control pickup class.
*/

#pragma once

#include "CoreMinimal.h"
#include "Pickup/IKA_Pickup_Base.h"
#include "IKA_Pickup_RemoteControlled.generated.h"

/**
 * 
 */
UCLASS()
class IKATEST_API AIKA_Pickup_RemoteControlled : public AIKA_Pickup_Base
{
	GENERATED_BODY()
	
public:
	/** Apply pickup to the pawn that collided with pickup. */
	virtual void PerformPickupAbility(APawn* collidedPawn) override;
	
};
