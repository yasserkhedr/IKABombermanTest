/**
	Faster run pick up class.
*/

#pragma once

#include "CoreMinimal.h"
#include "Pickup/IKA_Pickup_Base.h"
#include "IKA_Pickup_FasterRun.generated.h"

/**
 * 
 */
UCLASS()
class IKATEST_API AIKA_Pickup_FasterRun : public AIKA_Pickup_Base
{
	GENERATED_BODY()
	
public:
	AIKA_Pickup_FasterRun();

	/** Apply pickup to the pawn that collided with pickup. */
	virtual void PerformPickupAbility(APawn* collidedPawn);
	
	/** Player speed to be processed. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	float speedModification;
};
