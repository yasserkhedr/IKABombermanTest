/**
	More bombs pick up class.
*/

#pragma once

#include "CoreMinimal.h"
#include "Pickup/IKA_Pickup_Base.h"
#include "IKA_Pickup_MoreBombs.generated.h"

/**
 * 
 */
UCLASS()
class IKATEST_API AIKA_Pickup_MoreBombs : public AIKA_Pickup_Base
{
	GENERATED_BODY()
	
public:
	AIKA_Pickup_MoreBombs();
	
	/** Apply pickup to the pawn that collided with pickup. */
	virtual void PerformPickupAbility(APawn* collidedPawn) override;
	
	/** The bomb count to be processed. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	int32 bombCount;
};
