// Fill out your copyright notice in the Description page of Project Settings.

#include "IKA_Pickup_RemoteControlled.h"
#include "IKA_Pawn.h"

void AIKA_Pickup_RemoteControlled::PerformPickupAbility(APawn* collidedPawn)
{
	if (AIKA_Pawn* pPawn = Cast<AIKA_Pawn>(collidedPawn))
	{
		pPawn->StartRemoteControlMode();
	}
}
