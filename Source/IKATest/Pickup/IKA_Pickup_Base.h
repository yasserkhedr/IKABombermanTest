/** 
	Pick up base class, has the base virtual function to perform pickup ability.
*/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/BoxComponent.h"
#include "IKA_Pickup_Base.generated.h"

UCLASS()
class IKATEST_API AIKA_Pickup_Base : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIKA_Pickup_Base();
	
//	virtual void OnOverlapBegin(class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	
	/** Core function that activates each pickup's special effect. */
	UFUNCTION(BlueprintCallable, Category = "Player")
	virtual void PerformPickupAbility(APawn* collidedPawn);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
