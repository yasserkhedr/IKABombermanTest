// Fill out your copyright notice in the Description page of Project Settings.

#include "IKA_Pickup_MoreBombs.h"
#include "IKA_Pawn.h"

AIKA_Pickup_MoreBombs::AIKA_Pickup_MoreBombs() :
	bombCount(1)
{

}

void AIKA_Pickup_MoreBombs::PerformPickupAbility(APawn* collidedPawn)
{
	if (AIKA_Pawn* pPawn = Cast<AIKA_Pawn>(collidedPawn))
	{
		if (!pPawn->GetRemoteControlMode())
		{
			pPawn->ModifyBombCount(bombCount);
		}
	}
}
