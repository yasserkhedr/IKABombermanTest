// Fill out your copyright notice in the Description page of Project Settings.

#include "IKA_Pickup_LongerBlast.h"
#include "IKA_Pawn.h"

AIKA_Pickup_LongerBlast::AIKA_Pickup_LongerBlast() :
	blastRange(1)
{

}

void AIKA_Pickup_LongerBlast::PerformPickupAbility(APawn* collidedPawn)
{
	if (AIKA_Pawn* pPawn = Cast<AIKA_Pawn>(collidedPawn))
	{
		pPawn->ModifyBombRange(blastRange);
	}
}
