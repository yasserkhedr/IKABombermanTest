// Fill out your copyright notice in the Description page of Project Settings.

#include "IKA_Pickup_FasterRun.h"
#include "IKA_Pawn.h"

AIKA_Pickup_FasterRun::AIKA_Pickup_FasterRun() :
	speedModification(100.f)
{

}

void AIKA_Pickup_FasterRun::PerformPickupAbility(APawn* collidedPawn)
{
	if (AIKA_Pawn* pPawn = Cast<AIKA_Pawn>(collidedPawn))
	{
		pPawn->ModifyPlayerSpeed(speedModification);
	}
}
