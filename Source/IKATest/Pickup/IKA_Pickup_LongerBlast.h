/**
	Longer blast radius pick up class.
*/

#pragma once

#include "CoreMinimal.h"
#include "Pickup/IKA_Pickup_Base.h"
#include "IKA_Pickup_LongerBlast.generated.h"

/**
 * 
 */
UCLASS()
class IKATEST_API AIKA_Pickup_LongerBlast : public AIKA_Pickup_Base
{
	GENERATED_BODY()
	
public:
	AIKA_Pickup_LongerBlast();

	/** Apply pickup to the pawn that collided with pickup. */
	virtual void PerformPickupAbility(APawn* collidedPawn) override;
	
	/** The range of the blast to be processed. */
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player")
	float blastRange;
};
