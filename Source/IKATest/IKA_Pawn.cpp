// Fill out your copyright notice in the Description page of Project Settings.

#include "IKA_Pawn.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AIKA_Pawn::AIKA_Pawn():
_playerName(""),
playerNumber(1),
_playerBombRange(1),
playerSpeed(200.f),
_availableBombs(1),
_availableBombsHold(1),
_bPlayerHasRemoteControl(false),
_remoteModeTimer(10.f),
playerLives(3)
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AIKA_Pawn::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AIKA_Pawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AIKA_Pawn::StartRemoteControlMode()
{
	_bPlayerHasRemoteControl = true;

	_availableBombsHold = _availableBombs;

	_availableBombs = 1;

	UWorld* pWorld = GetWorld();

	if (!IsValid(pWorld))
		return;

	if (pWorld)
	{
		pWorld->GetTimerManager().SetTimer(_remoteTimer, this, &AIKA_Pawn::EndRemoteControlMode, _remoteModeTimer);
	}
}

void AIKA_Pawn::EndRemoteControlMode()
{
	if (_remoteBomb)
	{
		_remoteBomb->Detonate();
	}

	_bPlayerHasRemoteControl = false;

	_availableBombs = _availableBombsHold;
}

void AIKA_Pawn::ModifyBombCount(int32 countModificationValue)
{
	_availableBombs += countModificationValue;
}

void AIKA_Pawn::ModifyBombRange(int32 rangeModficationValue)
{
	_playerBombRange += rangeModficationValue;
}

void AIKA_Pawn::ModifyPlayerSpeed(float speedModificationValue)
{
	playerSpeed += speedModificationValue;
}

void AIKA_Pawn::PlayerKilled(APawn* instigator)
{
	if (playerLives > 0)
	{
		playerLives--;
	}

	if (Cast<AIKA_Pawn>(instigator))
	{
		if (!(Cast<AIKA_Pawn>(instigator) == this))
		{
			ModifyPlayerScore(500);
		}
	}

	RespawnPlayer();
}

void AIKA_Pawn::RespawnPlayer()
{
	ResetPlayerMovement();
	// Respawn player code.
	SetActorLocation(respawnPoint);
	
	currentLocation = respawnPoint;
}

void AIKA_Pawn::PlaceBomb()
{
	UWorld* pWorld = GetWorld();

	if (!IsValid(pWorld))
		return;

	if (_availableBombs <= 0)
		return;

	AIKA_Bomb* pBomb = pWorld->SpawnActorDeferred<AIKA_Bomb>(
		bombToUse,
		FTransform(currentLocation), this, this,
		ESpawnActorCollisionHandlingMethod::AlwaysSpawn
		);

	if (pBomb)
	{
		//Data injection
		pBomb->InitBombParameters(_playerBombRange, _bPlayerHasRemoteControl ? true : false, currentLocation);
		//Required for spawn actor deffered.
		UGameplayStatics::FinishSpawningActor(pBomb, FTransform(currentLocation));
	}

	if (_bPlayerHasRemoteControl)
	{
		_remoteBomb = pBomb;
	}
	
	ModifyBombCount(-1);
}

void AIKA_Pawn::DetonateRemoteBomb()
{
	if (_remoteBomb)
	{
		_remoteBomb->Detonate();
	}
}
