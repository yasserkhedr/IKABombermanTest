// Fill out your copyright notice in the Description page of Project Settings.

#include "IKA_Bomb.h"
#include "Walls/IKA_Wall_Base.h"
#include "Walls/IKA_DestructibleWall.h"
#include "IKA_Pawn.h"
#include "DrawDebugHelpers.h"
#include "Kismet/GameplayStatics.h"


// Sets default values
AIKA_Bomb::AIKA_Bomb() :
	_range(1),
	_remoteControlled(false),
	_bombTime(2.0f),
	_gridSizeIndex(100)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AIKA_Bomb::BeginPlay()
{
	Super::BeginPlay();

	UWorld* pWorld = GetWorld();
	if (pWorld && !_remoteControlled)
	{
		pWorld->GetTimerManager().SetTimer(bombTimer, this, &AIKA_Bomb::Detonate, _bombTime);
	}
}

// Called every frame
void AIKA_Bomb::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//SetActorLocation(_location);
}

void AIKA_Bomb::InitBombParameters(int32 range, bool remoteControlled, FVector spawnLocation)
{
	_range = range;
	_remoteControlled = (uint8)remoteControlled;
	_location = spawnLocation;
}

void AIKA_Bomb::Detonate()
{
	AIKA_Pawn* pPlayer = Cast<AIKA_Pawn>(GetOwner());
	if (pPlayer)
	{
		pPlayer->ModifyBombCount(1);
	}

	/** Not ideal bug fix - line trace back to yourself from end point to start
		to detect if player is still standing there. */
	SelfLineTrace();

	ExplosionLineTrace(_gridSizeIndex, 0, 50);
	ExplosionLineTrace(0, _gridSizeIndex, 50);
	ExplosionLineTrace(-_gridSizeIndex, 0, 50);
	ExplosionLineTrace(0, -_gridSizeIndex, 50);

	// Destroy, eventually GC will collect this.
	Destroy();
}

void AIKA_Bomb::SelfLineTrace()
{
	UWorld* pWorld = GetWorld();
	if (!IsValid(pWorld))
		return;

	FHitResult hitResult;
	FVector startLocation = _location + FVector(_gridSizeIndex / 2.f, _gridSizeIndex / 2.f, 50);
	FVector endLocation = _location + FVector(0, 0, 50);
	pWorld->LineTraceSingleByObjectType(hitResult, startLocation, endLocation, FCollisionObjectQueryParams::AllObjects);

	if (AIKA_Pawn* pPawn = Cast<AIKA_Pawn>(hitResult.GetActor()))
	{
		pPawn->PlayerKilled(Cast<APawn>(GetOwner()));
	}
}

void AIKA_Bomb::ExplosionLineTrace(int32 x, int32 y, int32 zOffset)
{
	UWorld* pWorld = GetWorld();
	if (!IsValid(pWorld))
		return;

	FVector startLocation, endLocation = FVector::ZeroVector;
	FVector adjustedLocation = _location;
	//startLocation = _location + FVector(x / 2.f, y / 2.f, zOffset);
	//endLocation = _location + FVector(x, y, zOffset);

	for (int32 currentRange = 0; currentRange < _range; currentRange++)
	{
		FHitResult hitResult;

		/** 
			The reason for the offset is because if I don't offset, the line trace either hits the floor
			or hits itself causing the line trace to fail. I could always add an ignore list with the other types
			of line traces but I don't want to use a collision channel I would rather use find by object types.
		*/
		startLocation = adjustedLocation + FVector(x / 2.f, y / 2.f, zOffset);
		endLocation = adjustedLocation + FVector(x, y, zOffset);
		adjustedLocation = endLocation;
		adjustedLocation.Z = 0.0f;

		pWorld->LineTraceSingleByObjectType(hitResult, startLocation, endLocation, FCollisionObjectQueryParams::AllObjects);

		if (AIKA_DestructibleWall* pWall = Cast<AIKA_DestructibleWall>(hitResult.GetActor()))
		{
			pWorld->SpawnActor<AActor>(explosionParticle, FTransform(adjustedLocation));
			pWall->DestroyWall(Cast<APawn>(GetOwner()));
			break;
		}
		
		if (Cast<AIKA_Wall_Base>(hitResult.GetActor()))
		{
			break;
		}
		
		pWorld->SpawnActor<AActor>(explosionParticle, FTransform(adjustedLocation));

		//DrawDebugLine(pWorld, startLocation, endLocation, FColor::Red, false, 15.f, 0, 2.f);

		if (AIKA_Bomb* pBomb = Cast<AIKA_Bomb>(hitResult.GetActor()))
		{
			pBomb->Detonate();
		}

		if (AIKA_Pawn* pPawn = Cast<AIKA_Pawn>(hitResult.GetActor()))
		{
			pPawn->PlayerKilled(Cast<APawn>(GetOwner()));
		}

		//Spawn effects
	}
}

