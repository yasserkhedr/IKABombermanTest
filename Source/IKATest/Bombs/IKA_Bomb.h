/**
	This is the base C++ class for the bombs.
	A blueprint file will be created that inherits from this C++ class.
*/


#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "IKA_Bomb.generated.h"

UCLASS()
class IKATEST_API AIKA_Bomb : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIKA_Bomb();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
	/** Init bomb values so it can behave according to the player's stats. */
	void InitBombParameters(int32 range, bool remoteControlled, FVector spawnLocation);

	/** Detonate bomb. (Explode) */
	void Detonate();

	/** This exists to solve a bug, this has to do with line tracing.
		Could have been handled better, but for now this is the fix.*/
	void SelfLineTrace();

	/** The line traces in which ever direction needed, also spawns the emitter explosions. */
	void ExplosionLineTrace(int32 x, int32 y, int32 zOffset);

	/** Getters and setters. */
	FORCEINLINE int32 GetRange() { return _range; };

	FORCEINLINE void SetRange(int32 range) { _range = range; };

	FORCEINLINE bool IsRemoteControlled() { return (bool)_remoteControlled; };

	FORCEINLINE void SetRemoteControlled(bool remoteControlled) { _remoteControlled = remoteControlled; };

	/** How long till the bomb explodes. */
	FTimerHandle bombTimer;

	/** Explosion particle. */
	UPROPERTY(EditAnywhere, Category = "Bomb")
	TSubclassOf<AActor> explosionParticle;

private:
	/** What is the range of the explosive, is it single range or double range or more? */
	int32 _range;

	/** Is it a remote controlled bomb? */
	uint8 _remoteControlled : 1;

	/** How long till bomb explodes. */
	float _bombTime;

	/** Location of the bomb */
	FVector _location;

	/** This was meant to be a global variable, but for the sake of this project it is fixed to 100. */
	int32 _gridSizeIndex;
};
