// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "IKATest.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, IKATest, "IKATest" );

DEFINE_LOG_CATEGORY(LogIKATest)
 