// Fill out your copyright notice in the Description page of Project Settings.

#include "IKA_Wall_Base.h"


// Sets default values
AIKA_Wall_Base::AIKA_Wall_Base()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AIKA_Wall_Base::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AIKA_Wall_Base::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

