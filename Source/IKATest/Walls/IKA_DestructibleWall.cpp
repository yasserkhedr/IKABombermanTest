// Fill out your copyright notice in the Description page of Project Settings.

#include "IKA_DestructibleWall.h"
#include "Kismet/KismetMathLibrary.h"
#include "IKA_Pawn.h"
#include "IKA_Gamemode.h"

AIKA_DestructibleWall::AIKA_DestructibleWall() :
	_wallLocation(FVector::ZeroVector),
	containsEnd(false),
	_wallValue(100)
{

}

// Called when the game starts or when spawned
void AIKA_DestructibleWall::BeginPlay()
{
	Super::BeginPlay();

	_wallLocation = GetActorLocation();
}

void AIKA_DestructibleWall::DestroyWall(APawn* instigator)
{
	UWorld* pWorld = GetWorld();

	if (!IsValid(pWorld))
		return;

	if (containsEnd)
	{
		FVector adjustedLocation = _wallLocation + FVector(0, 0, 1);
		AActor* pEnd = pWorld->SpawnActor<AActor>(endBlock, FTransform(adjustedLocation));
	}
	else
	{
		int32 randomNumber = UKismetMathLibrary::RandomIntegerInRange(1, 10);
		if (randomNumber <= 3)
		{
			int32 randomPickup = UKismetMathLibrary::RandomIntegerInRange(0, (pickUpsToDrop.Num() - 1));

			AIKA_Pickup_Base* pBomb = pWorld->SpawnActor<AIKA_Pickup_Base>(pickUpsToDrop[randomPickup], FTransform(_wallLocation));
		}
	}

	if (AIKA_Pawn* pPawn = Cast<AIKA_Pawn>(instigator))
	{
		pPawn->ModifyPlayerScore(_wallValue);
	}

	if (AIKA_Gamemode* pGamemode = Cast<AIKA_Gamemode>(GetWorld()->GetAuthGameMode()))
	{
		pGamemode->DecreaseBlocksLeft();
	}

	Destroy();
}

