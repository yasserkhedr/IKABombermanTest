/**
	This is the child class of the wall base class.
	I seperated this from the base class because it will hold data such as
	the wall's location which is an FVector, this is helps avoid having the indestructible
	wall hold the FVector value, which is a waste of memory (especially in terms of level generation). 
	This class will also handle extra fucntionality such as dropping items, there is not much 
	similarities between the destructible wall and the indestructible wall.
*/

#pragma once

#include "CoreMinimal.h"
#include "Walls/IKA_Wall_Base.h"
#include "Pickup/IKA_Pickup_Base.h"
#include "IKA_DestructibleWall.generated.h"

/**
 * 
 */
UCLASS()
class IKATEST_API AIKA_DestructibleWall : public AIKA_Wall_Base
{
	GENERATED_BODY()
	
public:
	AIKA_DestructibleWall();
	
	/** Destroys object when called. */
	void DestroyWall(APawn* instigator);
	
	/** Marker to indicate that wall will spawn the ending block. */
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	uint8 containsEnd : 1;

	/** List of pickups that can be dropped. */
	UPROPERTY(EditAnywhere)
	TArray<TSubclassOf<AIKA_Pickup_Base>> pickUpsToDrop;

	/** Piece of block the player stands on when the map is cleared to win the game. */
	UPROPERTY(EditAnywhere)
	TSubclassOf<AActor> endBlock;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	/** Used to spawn pickups etc. */
	FVector _wallLocation;

	/** How many points destroying a wall will net you. */
	int32 _wallValue;
};
