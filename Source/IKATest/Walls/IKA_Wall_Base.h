/**
	This is the base C++ class for the walls.
	For the sake of modularity and more power to expand the game, this class was added;
	with the simple requirements of the game this base class is not really needed, but
	I chose to still include it to expand if this game were to get more complicated
	requirements.
*/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "IKA_Wall_Base.generated.h"

UCLASS()
class IKATEST_API AIKA_Wall_Base : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AIKA_Wall_Base();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
